import npr

def fetchStreams(call_letters):
    streamList = []
    call_list = call_letters.split(",")
    for call in call_list:
        station = npr.Stations(call)
        stream = station.unwrap(station.mp3)
        streamList.append({'call':call,'stream':stream})
    return streamList